console.log("Hello, world!");
console.log("Hello, Batch203!");
console.
log(
	"Hello, everyone!"
);

// Comments: 

// This is a single line comment. ctrl + /

/* 

	This is a multiline comment ctrl + shift + /

*/ 

// Syntax and statements

// Syntax and Statements in programming are instruction that we tell the computer to perform 

// Syntax in programming, it is a set of rules that describe how statements must be constructed 

// Variables
/* 
	-it is used to contain data

	-Syntax in declaring variables
		-let/const varableNameSample;
*/

let myVariable = "Hello";

console.log(myVariable);

// console.log(hello);

// let	hello;

/*
	Guides in writing variables:
	1. Use the 'let' keywords followed by the variable name of your choosing and used the assignment operator (=) to assign a value.
	2.Variable names should start with a lowercase character, use camelCase for multiple words.
	3. For constant variables, use the 'const' keyword.
	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
	5. Never name a variable starting with numbers.
*/

/*Declaring variable*/

let	productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;



// let - we usually change/reassign the values of variables

// Reassigning variable values
// Syntax 
 	// variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend); // error: Identifier "friend" has already been declared.

// interest = 4.489; 
console.log(interest); // error because of assigning a value to a constant variable. 

// Reassigning variables vs initializing variables.
// Declared variable. Initialization is done after variable has been declared.
let supplier;
supplier = "John Smith Tradings";
console.log	(supplier);

supplier = "Zuitt Store";
console.log(supplier);

const pi = 3.1416
// pi = 3.1416; 
console.log (pi); //error due to const intialization

//var(ES1) vs let/const(ES6)
//let/const keyword avoid the hoisting of variable
// a = 5;
// console.log(a);
// var a; 

let	productCode = "DC017";
productBrand = "Dell";
console.log(productCode,productBrand)

// Using a variable with a reserved keyword.
// const let = "hello"; 
// console.log(let); // error: cannot used reserve keyword as a variable name

// [SECTION] Data types
// Strings 
// Series of charaters that create a word, a phrase, a sentence or anything related to creating text.


//Stings in JScript a signle ('') or double quote ("")
let country = 'Philippines';
let province = "Metro Manila";



// Concatenation Strings
//Multiple string values can be combined using +
let fullAddress = province + ',' + country;
console.log(fullAddress)

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character(\)
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress)

let message = "John's employees went home early"
console.log(message)

message = 'John\'s employees went home early';
console.log(message);


//Numbers
//Integers/Whole numbers
let headcount = 26;
console.log(headcount)

//Decimals numbers/fractions
let grade = 98.7;
console.log(grade);

//exponential notation
let planetDistance = 2e10;
console.log (planetDistance)

//combine number and strings
console.log ("John's grade last quarter is " + grade)

//Boolean
//true/false

let isMarried = false;
let isGoodConduct = true;
console.log ("isMarried" + isMarried);
console.log( "isGoodConduct" + isGoodConduct);

// Arrays 
// it is used to store multiple values with similar data type.
// Syntax:
 	// let/const arrayName = [elementA. elementB, elementC, ......]
 	let grades = [98.7 , 92.1 , 90.2 , 94.6]
 	console.log (grades);

 	//different date types
 	// storing different data types inside an array is not recommended because it will not make sense to in the context of programming.
 	let details = ["John" , "Smith" , 32 , true];
 	console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items.
// syntax
	/*
		let/const objectName = {
		propertyA: value,
		propertyB: value
		}
	*/

 	let person = {
 		fullName: "Juan Dela Cruz",
 		age: 35,
 		isMarried: false,
 		contact: ["+63917 123 4567", "8123 4567"],
 		address: {
 			houseNumber: "345",
 			city: "Manila"	
 		}
 	}

 	console.log(person)

 	//Create abstract object
 	let myGrades = {
 		firstGrading: 98.7,
 		secondGrading: 92.1,
 		thirdGrading:90.2,
 		fourthGrading: 94.6,
 	}


 	console.log	(myGrades)

 	console.log(typeof myGrades)

 	console.log (typeof grades)

 	// Constant objects and arrays
 	// We cannot reassign the value of the variable, but we can change the element of the constant array.

 	const anime = ["one piece" , "one punch man" , "attack on titan"];
 	anime[0] = ["kimetsu na yaiba"];

 	console.log(anime)

 	//Null
 	// It is used to intentionally express the absence of the value in a variable declaration/initialization

 	let spouse = null;
 	console.log (spouse);

 	let myNumber = 0; // number
 	let myString = ""; //string

 	// undefined value 
 	// Represents the state of a variable that has been declared but without an assigned value.
 	let fullName;
 	console.log (fullName);

